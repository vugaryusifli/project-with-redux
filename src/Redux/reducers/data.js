const initialState = null;

export default function data(state = initialState, action = {}) {
    switch (action.type) {
        case "GET_DATA":
            return action.data;
        default:
            return state;
    }
}