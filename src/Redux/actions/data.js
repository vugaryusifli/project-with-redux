import axios from "axios";

export const getData = () => async (dispatch) => {
    try {
        const payload = await axios.get("https://jsonplaceholder.typicode.com/posts")
            .then(res => res);

        if(payload.status === 200) {
            const generalData = {
                status: payload.status,
                data: payload.data
            }
            dispatch(dispatchData(generalData))
        }
    } catch (e) {
        console.log(e)
    }
}

export const postData = (userId, body, title) => async (dispatch) => {
    try {
        const payload = await axios.post("https://jsonplaceholder.typicode.com/posts",
            {
                userId: userId,
                title: title,
                body: body,
            },
            {
                headers: null
            }).then(res => res);

        if(payload.status === 200) {
            const generalData = {
                status: payload.status,
                data: payload.data
            }
            dispatch(dispatchData(generalData))
        }
    } catch (e) {
        console.log(e)
    }
}

export const putData = (id, userId, body, title) => async (dispatch) => {
    try {
        const payload = await axios.put("https://jsonplaceholder.typicode.com/posts/" + id,
            {
                id: id,
                userId: userId,
                title: title,
                body: body,
            },
            {
                headers: null
            }).then(res => res);

        if(payload.status === 200) {
            const generalData = {
                status: payload.status
            }
            dispatch(dispatchData(generalData))
        }
    } catch (e) {
        console.log(e)
    }
}

export const deleteData = (id) => async (dispatch) => {
    try {
        const payload = await axios.delete("https://jsonplaceholder.typicode.com/posts/" + id)
            .then(res => res);

        if(payload.status === 200) {
            const generalData = {
                status: payload.status
            }
            dispatch(dispatchData(generalData))
        }
    } catch (e) {
        console.log(e)
    }
}

export const dispatchData = (data) => ({
   type: "GET_DATA",
   data
});