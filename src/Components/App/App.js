import React from 'react';
import {Route, Switch} from "react-router-dom";
import Example from "../Example/ExampleContainer";

function App() {
  return (
    <Switch>
      <Route exact component={Example} path="/example"/>
    </Switch>
  );
}

export default App;
