import React from 'react';
import {
    Table,
    Modal,
    Button,
    Form,
    Row,
    Col,
    Spinner
} from "react-bootstrap";

function Example({
                     info,
                     _onChange,
                     addItem,
                     updateItem,
                     deleteItem,
                     onClose,
                     openModal
}) {

    function onChange(e, key) {
        _onChange(e.target.value, key)
    }

    return (
        <>
            <div className="mx-2 vh-100 d-flex align-items-center flex-column justify-content-center">
                {
                    info.loading ?
                        <Spinner animation="border" role="status">
                            <span className="sr-only">Loading...</span>
                        </Spinner>
                        :
                        <Table responsive hover striped bordered size="md">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th style={{minWidth:"100px"}}>User ID</th>
                                <th>Body</th>
                                <th>Title</th>
                                <th style={{minWidth:"150px"}}>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                info.data && info.data.map(item =>
                                    <tr key={item.id}>
                                        <th scope="row">{item.id}</th>
                                        <td>
                                            {item.userId}
                                        </td>
                                        <td>
                                            {item.body}
                                        </td>
                                        <td>
                                            {item.title}
                                        </td>
                                        <td>
                                            <div>
                                                <Button variant="warning" onClick={() => openModal(2, item.id)}>Edit</Button>
                                                {' '}
                                                <Button variant="danger" onClick={() => openModal(3, item.id)}>Del</Button>
                                            </div>
                                        </td>
                                    </tr>
                                )
                            }
                            </tbody>
                        </Table>
                }

                <div className="justify-content-start w-100">
                    <Button
                        className="mt-3"
                        variant="success"
                        onClick={() => openModal(1)}
                        type="button"
                        disabled={info.loading}
                    >
                        {
                            info.loading ?
                                <>
                                    <Spinner
                                        as="span"
                                        animation="border"
                                        size="sm"
                                        role="status"
                                        aria-hidden="true"
                                    />
                                    {' '}
                                    <span>Loading...</span>
                                </>
                                :
                                "Add item"
                        }
                    </Button>
                </div>

                <Modal show={info.show} onHide={onClose} centered>
                    <Modal.Header closeButton>
                        <Modal.Title>
                            {
                                info.openWithId === 1 ? "Add Item" : `Edit Item / ${info.itemId}`
                            }
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {
                            info.openWithId === 3 ?
                                `Are you sure to delete this content under id (${info.itemId})`
                                :
                                <Form>
                                    <Form.Group as={Row} controlId="formHorizontalEmail">
                                        <Form.Label column sm={2}>
                                            User Id
                                        </Form.Label>

                                        <Col sm={10}>
                                            <Form.Control type="number" placeholder="User Id" onChange={(e) => onChange(e, "userId")} />
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row}>
                                        <Form.Label column sm={2}>
                                            Body
                                        </Form.Label>
                                        <Col sm={10}>
                                            <Form.Control placeholder="Body" onChange={(e) => onChange(e, "body")} />
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row} className="mb-0">
                                        <Form.Label column sm={2}>
                                            Title
                                        </Form.Label>
                                        <Col sm={10}>
                                            <Form.Control placeholder="Title" onChange={(e) => onChange(e, "title")} />
                                        </Col>
                                    </Form.Group>
                                </Form>
                        }
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={onClose}>
                            Close
                        </Button>
                        {
                            info.openWithId === 3 ?
                                <Button variant="danger" onClick={deleteItem}>
                                    Delete
                                </Button>
                                :
                                <Button
                                    disabled={!info.userId || !info.body || !info.title}
                                    variant="success"
                                    onClick={
                                        info.openWithId === 1 ?
                                            addItem
                                            :
                                            updateItem
                                    }
                                >
                                    {
                                        info.openWithId === 1 ?
                                            "Add Item"
                                            :
                                            "Edit"
                                    }
                                </Button>
                        }

                    </Modal.Footer>
                </Modal>
            </div>
        </>
    );
}

export default Example;