import React, {useEffect, useState} from 'react';
import Example from "./Example";
import {connect} from "react-redux";
import {deleteData, getData, postData, putData} from "../../Redux/actions/data";

function ExampleContainer({data, getData, postData, putData, deleteData}) {

    const [info, setInfo] = useState({
        data: [],
        show: false,
        userId: "",
        body: "",
        title: "",
        loading: true,
        showStatusMessages: false
    });

    function _onChange(val, key) {
        setInfo({
            ...info,
            [key]: val
        });
    }

    function openModal(id, itemId) {
        setInfo({
            ...info,
            show: true,
            openWithId: id,
            itemId: itemId
        });
    }

    function addItem() {
        setInfo({
            ...info,
            show: false,
            loading: true
        });
        postData(info.userId, info.body, info.title);
    }

    function updateItem() {
        setInfo({
            ...info,
            show: false,
            loading: true
        });
        putData(info.itemId, info.userId, info.body, info.title);
    }

    function deleteItem(id) {
        setInfo({
            ...info,
            openWithId: id,
            show: false,
            loading: true
        });
        deleteData(info.itemId);
    }

    function onClose() {
        setInfo({
            ...info,
            show: false,
            userId: "",
            body: "",
            title: ""
        })
    }

    useEffect(() => {
        getData();
    }, [getData]);

    useEffect(() => {
        if(data && data.status === 200) {
            setInfo(info => ({
                ...info,
                data: data.data,
                loading: false
            }));
        }
    }, [data]);

    // useEffect(() => {
    //     console.log(info);
    // }, [info])

    return (
        <Example
            info={info}
            _onChange={_onChange}
            addItem={addItem}
            onClose={onClose}
            openModal={openModal}
            updateItem={updateItem}
            deleteItem={deleteItem}
        />
    );
}

function mapStateToProps(state) {
    return {
        data: state.data
    }
}

export default connect(mapStateToProps, {getData, postData, putData, deleteData})(ExampleContainer);